#ifndef __WIFIMGR_H__
#define __WIFIMGR_H__

#include "../config.h"
#include <Arduino.h>
#include <ArduinoOTA.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <WireGuard-ESP32.h>
#include <mdns.h>

static WireGuard wg;

class WiFiManager {
public:
  String getHostname() {
    String hostname = "iota-" + WiFi.macAddress();
    hostname.replace(":", "");
    hostname.toLowerCase();
    return hostname;
  }
  void setupWiFi() {
    String hostname = getHostname();
    WiFi.setHostname(hostname.c_str());
    Serial.printf("Setting hostname to %s\n", hostname.c_str());
    if (ENABLE_WIFI && ENABLE_WIFI_AP) {
      WiFi.mode(WIFI_MODE_APSTA);
    }
    if (ENABLE_WIFI) {
      WiFi.disconnect(true);
      delay(1000);
      WiFi.onEvent(
          [](WiFiEvent_t event, WiFiEventInfo_t info) {
            Serial.println("Wifi disconnected lambda...");
            WiFi.disconnect(true);
            delay(1000);
            WiFi.begin(wifi_ssid, wifi_password);
          },
          ARDUINO_EVENT_WIFI_STA_DISCONNECTED);

      Serial.println("Connecting to the AP...");
      WiFi.begin(wifi_ssid, wifi_password);
      int i = 0;
      while (!WiFi.isConnected()) {
        delay(1000);
        i++;
        Serial.print(".");
        if (i > 10) {
          Serial.println(
              "Could not connect to wifi after 10 seconds, rebooting");
          ESP.restart();
          break;
        }
      }
      Serial.println();
      Serial.printf("Connected, ip: ");
      Serial.println(WiFi.localIP());
      Serial.println("Adjusting system time...");
      configTime(9 * 60 * 60, 0, "time.google.com", "ntp.jst.mfeed.ad.jp",
                 "ntp.nict.jp");

      Serial.println("Connected");
    }
    if (ENABLE_WIFI_AP) {
      WiFi.softAP(wifi_ap_ssid, wifi_ap_password);
      Serial.printf("Setup AP, ip: ");
      Serial.println(WiFi.softAPIP());
    }
  }

  void setupMDNS() {
    String hostname = getHostname();
    // initialize mDNS service
    esp_err_t err = mdns_init();
    if (err) {
      printf("MDNS Init failed: %d\n", err);
      return;
    }

    // set hostname
    mdns_hostname_set(hostname.c_str());
  }

  void setupWireguard() {
    Serial.println("Connecting to wireguard");
    wg.begin(local_ip, private_key, endpoint_address, public_key,
             endpoint_port);
    Serial.print("Connected to wg, ip=");
    Serial.println(local_ip);
  }

  void setupOTA(int port = 3232) {
    Serial.println("Setting up OTA");
    // ArduinoOTA.setPassword((const char *)"passwordhere");
    ArduinoOTA.setPort(port);

    ArduinoOTA.onStart([]() { Serial.println("Start OTA"); });
    ArduinoOTA.onEnd([]() { Serial.println("OTA End"); });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("OTA Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
      Serial.printf("OTA Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR)
        Serial.println("OTA Auth Failed");
      else if (error == OTA_BEGIN_ERROR)
        Serial.println("OTA Begin Failed");
      else if (error == OTA_CONNECT_ERROR)
        Serial.println("OTA Connect Failed");
      else if (error == OTA_RECEIVE_ERROR)
        Serial.println("OTA Receive Failed");
      else if (error == OTA_END_ERROR)
        Serial.println("OTA End Failed");
    });
    ArduinoOTA.begin();
    Serial.println("OTA Setup Done");
  }
};

#endif // __WIFIMGR_H__