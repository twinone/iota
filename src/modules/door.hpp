#ifndef __DOOR__H__
#define __DOOR__H__

#include <Arduino.h>


#define PIN_DOOR_SWITCH 12
#define PIN_BUTTON 13
#define PIN_MOTOR_A 25
#define PIN_MOTOR_B 26

#define DOOR_MOTOR_OPEN 0
#define DOOR_MOTOR_CLOSE 1
#define DOOR_MOTOR_NONE -1

#define DOOR_SWITCH_CLOSED LOW


bool detectPinChange(int pin, int &val, int pull = INPUT_PULLDOWN) {
  int prevVal = val;
  pinMode(pin, pull);
  val = digitalRead(pin);
  if (val != prevVal) {
    delay(20);
    val = digitalRead(pin);
  }
  return val != prevVal;
}

int doorSwitchState = HIGH;
int buttonSwitchState = HIGH;

int motorDirection = DOOR_MOTOR_NONE;
int motorState = DOOR_MOTOR_NONE;

void motor(int direction, int timeout = 7000) {
  Serial.printf("Direction, %d\n", direction);
  pinMode(PIN_MOTOR_A, INPUT);
  pinMode(PIN_MOTOR_B, INPUT);
  delay(20);

  if (direction == DOOR_MOTOR_CLOSE) {
    pinMode(PIN_MOTOR_B, OUTPUT);
    digitalWrite(PIN_MOTOR_B, LOW);
  }
  if (direction == DOOR_MOTOR_OPEN) {
    pinMode(PIN_MOTOR_A, OUTPUT);
    digitalWrite(PIN_MOTOR_A, LOW);
  }

  delay(timeout);

  pinMode(PIN_MOTOR_A, INPUT);
  pinMode(PIN_MOTOR_B, INPUT);

  motorDirection = direction;
  motorState = direction;
}

void handleDoor() {
  // button
  if (detectPinChange(PIN_BUTTON, buttonSwitchState, INPUT_PULLUP)) {
    Serial.printf("Button %d\n", buttonSwitchState);
    if (buttonSwitchState == HIGH) {
      motor(motorDirection == DOOR_MOTOR_CLOSE ? DOOR_MOTOR_OPEN
                                               : DOOR_MOTOR_CLOSE,
            3000);
    }
  }

  // door
  if (detectPinChange(PIN_DOOR_SWITCH, doorSwitchState, INPUT_PULLUP)) {
    Serial.printf("Door, %d\n", doorSwitchState);
    if (doorSwitchState == DOOR_SWITCH_CLOSED) {
      motor(DOOR_MOTOR_CLOSE);
    }
  }
}





#endif // __DOOR__H__