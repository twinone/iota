#ifndef __BUILDING__H__
#define __BUILDING__H__

#include <Arduino.h>



void openBuilding(int pin = 22, int ms = 3000) {
  pinMode(pin, OUTPUT);
  digitalWrite(pin, HIGH);
  delay(ms);
  digitalWrite(pin, LOW);
}



#endif // __BUILDING__H__