#include "interp.h"
#include "modules/building.hpp"
#include "modules/door.hpp"
#include "modules/wifi.hpp"
#include <Arduino.h>
#include <analogWrite.h>

Interp interp;
WiFiManager wifi;

void processCommand() {
  if (!Serial.available())
    return;
  // Serial.setTimeout(300); // better type fast or use an interfacer program ;)
  String line = Serial.readStringUntil('\n');
  Serial.println(interp.process(line));
}

void setup() {
  delay(10);
  Serial.begin(115200);
  delay(1000);

  Serial.println("iota");

  openBuilding();
  wifi.setupWiFi();
  wifi.setupMDNS();
  wifi.setupWireguard();
  // wifi.setupOTA();
}

void loop() {
  processCommand();
  delay(1);
}